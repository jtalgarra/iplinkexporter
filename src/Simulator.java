import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.evs.iplink.exporter.ExportItem;
import com.evs.iplink.exporter.Status;

import de.moovit.inyourplace.serializer.XStreamSerializer;

public class Simulator {

	public static void main(String[] args) {
		final SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy kk:mm:ss");

		final ArrayList<ExportItem> arrayList = new ArrayList<>();
		
		
		arrayList.add(new ExportItem("Sequence1", "xyz", "Web", df.format(new Date()), "", Status.SUCCESS));
		arrayList.add(new ExportItem("Sequence2", "xyza", "Playout", df.format(new Date()), "", Status.SUCCESS));
		final Random random = new Random();
		final File file = new File("/Users/Shared/Library/Application Support/Adobe/IPLink/Exporter/exportprogress.xml");
		try {
			XStreamSerializer.serialize(file, arrayList, false);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
		executorService.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
			
				try {
					for (ExportItem exportItem : arrayList) {
						exportItem.setExporterPhase(Status.values()[random.nextInt(3)]);
					}
					XStreamSerializer.serialize(file, arrayList, false);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}, 0, 3, TimeUnit.SECONDS);

	}
}
