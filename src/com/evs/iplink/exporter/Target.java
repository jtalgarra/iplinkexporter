package com.evs.iplink.exporter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public class Target {

	UUID uuid;
	String name, label;

	ArrayList<MetadataField> metadatafields;

	private Destination destination;

	public Target(UUID uuid, String name, String label, ArrayList<MetadataField> metadatafields, Destination destination) {
		super();
		this.uuid = uuid;
		this.name = name;
		this.label = label;
		this.metadatafields = metadatafields;
		this.destination = destination;
		uuid = UUID.randomUUID();
	}
	
	public Target(){
		uuid = UUID.randomUUID();
	}

	public UUID getUuid() {

		return uuid;
	}

	public Destination getDestination() {
		
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public ArrayList<MetadataField> getMetadatafields() {
		if (metadatafields == null)
			metadatafields = new ArrayList<>();
		return metadatafields;
		
	}

	public void setMetadatafields(ArrayList<MetadataField> metadatafields) {
		this.metadatafields = metadatafields;
	}

}