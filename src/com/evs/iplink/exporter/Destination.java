package com.evs.iplink.exporter;

import java.awt.Component;
import java.util.ArrayList;
import java.util.UUID;

public class Destination {
 
	ArrayList<MetadataField> metadatafields;
	
	public Destination(UUID uuid, String name, String path, String ext, ArrayList<MetadataField> metadatafields) {
		super();
		this.uuid = UUID.randomUUID();
		this.name = name;
		this.path = path;
		this.ext = ext;
		this.metadatafields = metadatafields;
	};

	UUID uuid;
	String name, path, ext;

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		path = null;
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public ArrayList<MetadataField> getMetadatafields() {
		
		return metadatafields;
		
	}

	public void setMetadatafields(ArrayList<MetadataField> metadatafields) {
		
		
		this.metadatafields = metadatafields;
	}

	

	

}
