package com.evs.iplink.exporter;

import java.util.ArrayList;
import java.util.UUID;

public class Profile {

	UUID uuid;
	String name, version, xsquareVersion, ipdVersion;

	ArrayList<MetadataField> metadatafields;
	ArrayList<Target> targets;

	public Profile(String name, String version, String xsquareVersion, String ipdVersion,
			ArrayList<MetadataField> metadatafields, ArrayList<Target> targets) {
		super();
		this.name = name;
		this.version = version;
		this.xsquareVersion = xsquareVersion;
		this.ipdVersion = ipdVersion;
		this.metadatafields = metadatafields;
		this.targets = targets;
		uuid = UUID.randomUUID();
	}

	public Profile() {
		uuid = UUID.randomUUID();
	}

	public ArrayList<Target> getTargets() {
		if (targets == null)
			targets = new ArrayList<>();
		return targets;
	}

	public void setTargets(ArrayList<Target> targets) {
		this.targets = targets;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getXsqaureVersion() {
		return xsquareVersion;
	}

	public void setXsqaureVersion(String xsqaureVersion) {
		this.xsquareVersion = xsqaureVersion;
	}

	public String getIpdVersion() {
		return ipdVersion;
	}

	public void setIpdVersion(String ipdVersion) {
		this.ipdVersion = ipdVersion;
	}

	public ArrayList<MetadataField> getMetadatafields() {
		if (metadatafields == null)
			metadatafields = new ArrayList<>();
		return metadatafields;
	}

	public void setMetadatafields(ArrayList<MetadataField> metadatafields) {
		this.metadatafields = metadatafields;
	}

	

}
