package com.evs.iplink.exporter;

import java.util.ArrayList;
import java.util.UUID;

public class MetadataField {
	UUID uuid;
	String name, label;
	String[] value;
	MetadataType metadataType;
	boolean editable = true;
	boolean mandatory;

	public MetadataField(String name, String label, String[] value, MetadataType metadataType, boolean mandatory) {
		super();
		this.name = name;
		this.label = label;
		this.value = value;
		this.metadataType = metadataType;
		this.mandatory = mandatory;
		uuid = UUID.randomUUID();

	}
	
	public MetadataField() {
		uuid = UUID.randomUUID();
	}

	public MetadataType getMetadataType() {
		return metadataType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String[] getValue() {
		return value;
	}

	public void setValue(String[] value) {
		this.value = value;
	}

	public void setMetadataType(MetadataType metadataType) {
		this.metadataType = metadataType;
	}

}
