package com.evs.iplink.exporter;

public class ExportItem {
	
	
	String taskName, varID, target, started, info;
	

	
	Status exporterPhase;
	
	



	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getStarted() {
		return started;
	}

	public void setStarted(String started) {
		this.started = started;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public ExportItem(String taskName, String varID, String target, String started, String info, Status exporterPhase) {
		this.taskName = taskName;
		this.varID = varID;
		this.target = target;
		this.started = started;
		this.info = info;
		this.exporterPhase = exporterPhase;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getVarID() {
		return varID;
	}

	public void setVarID(String varID) {
		this.varID = varID;
	}



	public Status getExporterPhase() {
		return exporterPhase;
	}

	public void setExporterPhase(Status exporterPhase) {
		this.exporterPhase = exporterPhase;
	}
	

}
