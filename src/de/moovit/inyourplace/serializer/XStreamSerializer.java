package de.moovit.inyourplace.serializer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;
import com.thoughtworks.xstream.io.xml.JDomDriver;
import com.thoughtworks.xstream.io.xml.XppDomDriver;

public class XStreamSerializer {

	static XStream xstream = new XStream(new JDomDriver());

	public static XStream getXstream() {
		return xstream;
	}

	@SuppressWarnings("unchecked")
	public static <T> T deserialize(InputStream in, Class<T> clazz, boolean closeStream) throws IOException {
		T toReturn = (T) xstream.fromXML(new BufferedInputStream(in));
		if (closeStream) {
			in.close();
		}
		return toReturn;
	}

	public static Object deserialize(File file) {

		try {
			return xstream.fromXML(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
		} catch (StreamException e) {
			try {
				Thread.sleep(300);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			deserialize(file);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public Object deserialize(String string) {

		try {
			return xstream.fromXML(string);
		} catch (StreamException e) {
			try {
				Thread.sleep(300);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		return null;

	}

	public String serialize(Object o) throws IOException {

		return xstream.toXML(o);

	}

	public static boolean serialize(Object o, File file, boolean deleteOnExit) throws IOException {
		return serialize(file, o, deleteOnExit);
	}

	public static boolean serialize(File file, Object o, boolean deleteOnExit) throws IOException {
		if (file.getParentFile() != null && !file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}

		

		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");

		writer.write(xstream.toXML(o));
		writer.close();
		return true;

	}

}
