import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import com.evs.iplink.exporter.MetadataField;
import com.evs.iplink.exporter.MetadataType;
import com.evs.iplink.exporter.Profile;
import com.evs.iplink.exporter.Target;

import de.moovit.inyourplace.serializer.XStreamSerializer;

public class ProfileCreator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Profile profile = new Profile();
		profile.setIpdVersion("06.20.78");
		profile.setName("WC Export");
		profile.setVersion("0.1");
		profile.setXsqaureVersion("x.x.x");

		profile.getMetadatafields().add(new MetadataField("name", "Name", new String[] { "$sequencename" }, MetadataType.TEXT, true));

		profile.getMetadatafields().add(new MetadataField("varID", "Var ID", new String[] { "$sequencename" }, MetadataType.TEXT, true));

		profile.getMetadatafields().add(new MetadataField("assetType", "Type", new String[] { "File", "XT Clip" }, MetadataType.DROPDOWN, true));

		profile.getMetadatafields().add(new MetadataField("createdDate", "Created", new String[] { "$today" }, MetadataType.DATE, true));

		ArrayList<MetadataField> targetMetadata1 = new ArrayList<>();
		targetMetadata1.add(new MetadataField("game", "Game", new String[] { "M1", "M2", "M3", "M4" }, MetadataType.DROPDOWN, false));
		targetMetadata1.add(new MetadataField("athelete", "Athelete", new String[] { "Player1", "Player2", "Player3", "Player4" }, MetadataType.DROPDOWN, true));
		targetMetadata1.add(new MetadataField("matchTime", "Match Time", new String[] { "now" }, MetadataType.TEXT, false));


		ArrayList<MetadataField> targetMetadata2 = new ArrayList<>();
		targetMetadata2.add(new MetadataField("game", "Game", new String[] { "M1", "M2", "M3", "M4" }, MetadataType.DROPDOWN, false));
		targetMetadata2.add(new MetadataField("matchTime", "Match Time", new String[] { "now" }, MetadataType.TEXT, false));

		//profile.getTargets().add(
				//new Target(UUID.randomUUID(), "xt1", "Playout", "/Volumes/RAID/Temp/IPLink OUT",
						//"/Users/Shared/Library/Application Support/Adobe/IPLink/exportSettings/AVC-Intra Class50 1080 50i.epr", "mov", targetMetadata1));
		
		//profile.getTargets().add(
				//new Target(UUID.randomUUID(), "xt1", "Web", "/Volumes/RAID/Temp/IPLink OUT", "/Users/Shared/Library/Application Support/Adobe/IPLink/exportSettings/HD 1080i 25.epr", "mp4",
						//targetMetadata2));

		try {
			XStreamSerializer.serialize(new File("/Users/Shared/Library/Application Support/Adobe/IPLink/exportprofiles", profile.getUuid().toString() + ".xml"), profile, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
